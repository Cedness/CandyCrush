//
// Created by ced on 28.05.21.
//

#include "Position.h"
#include <cmath>

template<typename T>
T pow2(T value) {
    return value * value;
}

const Position Position::INVALID = Position(-1, -1);

const std::array<Position, 4> Position::DIRECTIONS = std::array<Position, 4>{
        Position(0, 1),
        Position(1, 0),
        Position(0, -1),
        Position(-1, 0)
};

Position::Position()
        : Position(0, 0) {}

Position::Position(int x, int y)
        : x_(x), y_(y) {}

auto Position::x() const -> int {
    return x_;
}

void Position::x(int x) {
    x_ = x;
}

auto Position::y() const -> int {
    return y_;
}

void Position::y(int y) {
    y_ = y;
}

auto Position::operator-() const -> Position {
    return Position(-x(), -y());
}

auto Position::operator+(Position rhs) const -> Position {
    return Position(x() + rhs.x(), y() + rhs.y());
}

auto Position::operator-(Position rhs) const -> Position {
    return Position(x() - rhs.x(), y() - rhs.y());
}

auto Position::operator*(Position rhs) const -> Position {
    return Position(x() * rhs.x(), y() * rhs.y());
}

auto Position::operator+(int value) const -> Position {
    return Position(x() + value, y() + value);
}

auto Position::operator-(int value) const -> Position {
    return Position(x() - value, y() - value);
}

auto Position::operator+=(Position rhs) -> Position {
    x(x() + rhs.x());
    y(y() + rhs.y());
    return *this;
}

auto Position::operator-=(Position rhs) -> Position {
    x(x() - rhs.x());
    y(y() - rhs.y());
    return *this;
}

auto Position::abs() const -> Position {
    return Position(std::abs(x()), std::abs(y()));
}

auto Position::operator==(const Position& rhs) const -> bool {
    return x() == rhs.x() &&
           y() == rhs.y();
}

auto Position::operator!=(const Position& rhs) const -> bool {
    return !(rhs == *this);
}

auto Position::operator==(int value) const -> bool {
    return x() == value && y() == value;
}

auto Position::operator!=(int value) const -> bool {
    return !(*this == value);
}

auto Position::operator<(int value) const -> bool {
    return x() < value && y() < value;
}

auto Position::operator>(int value) const -> bool {
    return x() > value && y() > value;
}

auto Position::operator<=(int value) const -> bool {
    return x() <= value && y() <= value;
}

auto Position::operator>=(int value) const -> bool {
    return x() >= value && y() >= value;
}

auto Position::lengthNaive() const -> int {
    return std::abs(x()) + std::abs(y());
}

auto Position::lengthSquared() const -> int {
    return pow2(x()) + pow2(y());
}

auto Position::length() const -> double {
    return std::sqrt(lengthSquared());
}

auto Position::isValid() const -> bool {
    return *this != INVALID;
}

auto Position::inBounds(int gameFieldSize) const -> bool {
    return *this >= 0 && *this < gameFieldSize;
}

auto Position::toIndex(int gameFieldSize) const -> int {
    return gameFieldSize * y() + x();
}

auto operator<<(std::ostream& ostream, const Position& position) -> std::ostream& {
    if (position.isValid())
        return ostream << position.x() + 1 << '/' << position.y() + 1;
    else
        return ostream;
}
