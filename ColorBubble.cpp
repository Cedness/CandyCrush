//
// Created by Ced on 17/07/2021.
//

#include "ColorBubble.h"

ColorBubble::ColorBubble(Position position) : SpecialBubble(position, BLACK) {}

auto ColorBubble::isTwoStepAction() const -> bool {
    return true;
}

auto ColorBubble::performSecondClickAction(GameField& gameField, Bubble& firstClick) -> std::pair<double, std::vector<Deletion>> {
    std::vector<Deletion> deletions{ getPosition() };
    Color color = firstClick.getColor();
    for (Position current; current.y() < gameField.size(); current.y(current.y() + 1)) {
        for (current.x(0); current.x() < gameField.size(); current.x(current.x() + 1)) {
            if (gameField[current].getColor() != color)
                continue;
            deletions.emplace_back(current);
        }
    }
    return { 5, deletions };
}
