//
// Created by Ced on 26/07/2021.
//

#include "Deletion.h"

Deletion::Deletion(Position position) : position_(position) {}

Position Deletion::getSource() const {
    return position_;
}
