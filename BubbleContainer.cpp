//
// Created by Ced on 16/07/2021.
//

#include "BubbleContainer.h"
#include <iostream>

BubbleContainer::BubbleContainer(int size, std::function<Bubble*(Position)> bubbleGenerator)
        : size_(size) {
    fill(bubbleGenerator);
}

BubbleContainer::~BubbleContainer() {
    clear();
}

void BubbleContainer::refill(std::function<Bubble*(Position)> bubbleGenerator) {
    clear();
    fill(bubbleGenerator);
}

void BubbleContainer::fill(std::function<Bubble*(Position)> bubbleGenerator) {
    bubbles_.reserve(size_ * size_);
    std::array<int, DEFAULT_COLOR_SIZE> colorCount{};
    for (int index = 0, y = 0; y < size_; y++) {
        for (int x = 0; x < size_; x++, index++) {
            Bubble* bubble = bubbleGenerator(Position(x, y));
            bubbles_.push_back(bubble);
            colorCount[bubble->getColor()]++;
        }
    }
    // double average = 0;
    for (int i = 0; i < colorCount.size(); i++) {
        int count = colorCount[i];
        std::cout << colorAsLetter(static_cast<Color>(i)) << ": " << count << '\n';
        // double next = i + 1;
        // average = (static_cast<double>(i) / next * average) + static_cast<double>(count) / next;
    }
    // std::cout << "average: " << average << '\n';
}

void BubbleContainer::clear() {
    for (int index = 0, y = 0; y < size_; y++) {
        for (int x = 0; x < size_; x++, index++) {
            if (isBubbleAt(index)) {
                delete bubblePtrAt(index);
            }
        }
    }
    bubbles_.clear();
}

auto BubbleContainer::bubbleCount() const -> int {
    return static_cast<int>(bubbles_.size());
}

auto BubbleContainer::isBubbleAt(int index) -> bool {
    return bubblePtrAt(index) != nullptr;
}

auto BubbleContainer::isBubbleAt(Position position) -> bool {
    return bubblePtrAt(position) != nullptr;
}

auto BubbleContainer::bubblePtrAt(int index) -> Bubble*& {
    return bubbles_[index];
}

auto BubbleContainer::bubblePtrAt(Position position) -> Bubble*& {
    return bubblePtrAt(position.toIndex(size_));
}

auto BubbleContainer::bubbleAt(int index) -> Bubble& {
    return *bubblePtrAt(index);
}

auto BubbleContainer::operator[](int index) -> Bubble& {
    return bubbleAt(index);
}

auto BubbleContainer::bubbleAt(Position position) -> Bubble& {
    return bubbleAt(position.toIndex(size_));
}

auto BubbleContainer::operator[](Position position) -> Bubble& {
    return bubbleAt(position);
}

void BubbleContainer::doSwap(int index1, int index2) {
    Bubble* bubble1 = bubblePtrAt(index1);
    Bubble* bubble2 = bubblePtrAt(index2);
    bubblePtrAt(index1) = bubble2;
    bubblePtrAt(index2) = bubble1;
    Position position1 = bubble1->getPosition();
    Position position2 = bubble2->getPosition();
    bubble1->setPosition(position2);
    bubble2->setPosition(position1);
}
