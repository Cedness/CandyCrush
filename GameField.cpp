//
// Created by ced on 28.05.21.
//

#include "GameField.h"
#include "CCException.h"
#include "SquareBubble.h"
#include "RowColumnBubble.h"
#include "ColorBubble.h"
#include <array>
#include <queue>
#include <algorithm>
#include <iostream>

GameField::GameField(int size)
        : GameField(size, [](GameField& g) {}) {}

GameField::GameField(int size, std::function<void (GameField& g)> refreshCallback)
        : size_(size), bubbles_(std::make_unique<BubbleContainer>(size_, [this](auto p) -> Bubble* { return createBubble(p); })), refreshCallback_(std::move(refreshCallback)) {
    if (size_ < 3)
        throw std::out_of_range("game field size must be at least 3");
    refreshCallback_(*this);
    deleteAndRefill();
    timeLeft_ = START_TIME;
    points_ = 0;
    if (!hasMovesLeft_)
        initUntilDone();
}

GameField& GameField::operator=(GameField&& gameField) noexcept {
    size_ = gameField.size_;
    gen_ = gameField.gen_;
    bubbles_ = std::move(gameField.bubbles_);
    timeLeft_ = gameField.timeLeft_;
    specialBubbleCount_ = gameField.specialBubbleCount_;
    hasMovesLeft_ = gameField.hasMovesLeft_;
    latestResult_ = gameField.latestResult_;
    points_ = gameField.points_;
    refreshCallback_ = gameField.refreshCallback_;
    selection_ = gameField.selection_;
    return *this;
}

void GameField::initUntilDone() {
    do {
        std::cout << "Resetting..." << std::endl;
        bubbles_->refill([this](auto p) -> Bubble* { return createBubble(p); });
        latestResult_ = std::make_shared<ClickResult>();
        deleteAndRefill();
    } while (!hasMovesLeft_);
    timeLeft_ = START_TIME;
    points_ = 0;
}

void GameField::reset() {
    selection_ = Position::INVALID;
    timeLeft_ = 20.0;
    initUntilDone();
}

auto GameField::size() const -> int {
    return size_;
}

auto GameField::isBubbleAt(Position position) -> bool {
    return bubbles_->isBubbleAt(position);
}

auto GameField::bubbleAt(Position position) -> const Bubble& {
    return bubbles_->bubbleAt(position);
}

auto GameField::operator[](Position position) -> const Bubble& {
    return bubbles_->bubbleAt(position);
}

void GameField::tick() {
    if (!isGameOver())
        timeLeft_ = std::max(0.0, timeLeft_ - 1.0);
}

auto GameField::getTimeLeft() const -> int {
    return static_cast<int>(timeLeft_);
}

auto GameField::isGameOver() const -> bool {
    return !hasMovesLeft_ || timeLeft_ <= 0;
}

auto GameField::hasMovesLeft() const -> bool {
    return hasMovesLeft_;
}

auto GameField::getLatestResult() const -> std::shared_ptr<const ClickResult> {
    return latestResult_;
}

auto GameField::getPoints() const -> int {
    return points_;
}

auto GameField::clickedAt(Position position) -> std::shared_ptr<const ClickResult> {
    try {
        if (isGameOver())
            throw CCException("Game Over");
        latestResult_ = std::make_shared<ClickResult>();
        latestResult_->getAddedPoints() = points_;
        latestResult_->getAddedTime() = timeLeft_;

        if (!position.isValid() || position == selection_) {
            std::cout << "Deselected" << std::endl;
            selection_ = Position::INVALID;
        } else if (!position.inBounds(size_)) {
            throw std::out_of_range("Position out of range");
        } else {
            performClickAction(position);
        }
        latestResult_->getAddedPoints() = points_ - latestResult_->getAddedPoints();
        latestResult_->getAddedTime() = timeLeft_ - latestResult_->getAddedTime();
        latestResult_->isSelected() = selection_.isValid();
        return latestResult_;
    } catch (CCException& ex) {
        selection_ = Position::INVALID;
        throw ex;
    }
}

void GameField::performClickAction(Position position) {
    Bubble& bubble = bubbles_->bubbleAt(position);
    if (!selection_.isValid()) { // First click
        if (bubble.hasSpecialAction() && !bubble.isTwoStepAction()) {
            specialAction(bubble, nullptr);
        } else {
            std::cout << "Selected" << std::endl;
            selection_ = position;
            return;
        }
    } else { // Second click
        Bubble& firstBubble = bubbles_->bubbleAt(selection_);
        if (firstBubble.hasSpecialAction()) {
            specialAction(firstBubble, &bubble);
#if REVERSE_CLICK_COLOR_BUBBLE
        } else if (bubble.hasSpecialAction() && bubble.isTwoStepAction()) {
            specialAction(bubble, &firstBubble);
#endif
        } else {
            swap(selection_, position);
        }
    }
    selection_ = Position::INVALID;
}

void GameField::specialAction(Bubble& specialBubble, Bubble* otherBubble) {
    std::cout << "Special action" << std::endl;
    auto[addedTime, deletions] = otherBubble == nullptr ? specialBubble.performFirstClickAction(*this) : specialBubble.performSecondClickAction(*this, *otherBubble);
    timeLeft_ += addedTime;
    latestResult_->getChangeCycles().emplace_back(std::move(deletions), std::vector<Movement>(), std::vector<Refill>());
    deleteAndRefill();
}

void GameField::swap(Position position1, Position position2) {
    std::cout << "Swap " << position1 << "|" << position2 << std::endl;
    std::array<Position, 2> positions{ position1, position2 };
    std::array<int, 2> indices{
            position1.toIndex(size_),
            position2.toIndex(size_)
    };

    auto[rowLengths, maxRowLengths, bubbles] = checkUserSwap(positions, indices);

    latestResult_->getSwap() = Movement(position1, position2);
    refreshCallback_(*this);

    auto upgradedBubbles = createUpgradedBubbles(latestResult_->getUpgrades(), positions, bubbles, rowLengths, maxRowLengths);

    deleteAndRefill(bubbles, upgradedBubbles);
}

auto GameField::checkUserSwap(std::array<Position, 2>& positions, std::array<int, 2>& indices) -> std::tuple<std::array<std::array<int, 2>, 2>, std::array<int, 2>, std::array<Bubble*, 2>> {
    if ((positions[1] - positions[0]).lengthNaive() > 1)
        throw CCException("Die Positionen sind keine Nachbarn!");

    auto[allowed, rowLengths, maxRowLengths, bubbles] = checkSwap(positions, indices, false);

    if (!allowed)
        throw CCException("Ein Tausch muss eine 3er-Reihe bilden!");

    return { rowLengths, maxRowLengths, bubbles };
}

auto GameField::checkSwap(std::array<Position, 2>& positions, std::array<int, 2>& indices, bool undo) -> std::tuple<bool, std::array<std::array<int, 2>, 2>, std::array<int, 2>, std::array<Bubble*, 2>> {
    bubbles_->doSwap(indices[0], indices[1]);

    std::array<Bubble*, 2> bubbles{
            bubbles_->bubblePtrAt(indices[0]),
            bubbles_->bubblePtrAt(indices[1])
    };

    std::array<std::array<int, 2>, 2> rowLengths;
    for (auto& item : rowLengths) {
        item.fill(1);
    }
    std::array<int, 2> maxRowLengths;
    maxRowLengths.fill(1);
    for (int i = 0; i < 2; i++) {
        Position root = positions[i];
        Color color = bubbles[i]->getColor();
        for (int xMul = 0, yMul = 1; xMul < 2; xMul++, yMul--) { // switch vertical/horizontal walking
            for (int direction = -1; direction <= 1; direction += 2) { // switch walking direction
                Position step = Position(xMul * direction, yMul * direction);
                Position current = root;
                while (rowLengths[i][xMul] < MAX_ROW_LENGTH) { // walk
                    current += step;
                    if (!(current >= 0) || !(current < static_cast<int>(size_)) || bubbles_->bubbleAt(current).getColor() != color) // out of bounds or no more equal colors found
                        break;
                    // more equal colors found
                    rowLengths[i][xMul]++;
                    if (rowLengths[i][xMul] > maxRowLengths[i])
                        maxRowLengths[i] = rowLengths[i][xMul];
                }
            }
        }
    }

    bool allowed = !(maxRowLengths[0] < MIN_ROW_LENGTH && maxRowLengths[1] < MIN_ROW_LENGTH);
    if (undo || !allowed) {
        bubbles_->doSwap(indices[0], indices[1]);
    }
    return { allowed, rowLengths, maxRowLengths, bubbles };
}

auto GameField::createUpgradedBubbles(std::vector<Refill>& upgrades, std::array<Position, 2>& positions, std::array<Bubble*, 2>& bubbles, std::array<std::array<int, 2>, 2>& rowLengths, std::array<int, 2>& maxRowLengths) -> std::array<Bubble*, 2> {
    std::array<Bubble*, 2> upgradedBubbles{};
    for (int i = 0; i < 2; i++) {
        Bubble* specialBubble;
        Position position = bubbles[i]->getPosition();
        // Only constellations that resulted directly from the click shall be considered for making special bubbles
        switch (maxRowLengths[i]) {
            case 5:
                specialBubble = new ColorBubble(position);
                break;
            case 4:
                specialBubble = new RowColumnBubble(position, rowLengths[i][0] < rowLengths[i][1]);
                break;
            case 3:
                if (rowLengths[i][0] >= MIN_ROW_LENGTH && rowLengths[i][1] >= MIN_ROW_LENGTH) {
                    specialBubble = new SquareBubble(position);
                    break;
                } // No break here is intended to trigger continue
            default:
                continue;
        }
        upgradedBubbles[i] = specialBubble;
        upgrades.emplace_back(positions[i], positions[i], specialBubble->getColor());
        specialBubbleCount_++;
    }
    return upgradedBubbles;
}

void GameField::deleteAndRefill() {
    std::array<Bubble*, 2> bubbles{};
    std::array<Bubble*, 2> upgrades{};
    deleteAndRefill(bubbles, upgrades);
}

void GameField::deleteAndRefill(std::array<Bubble*, 2>& bubbles, const std::array<Bubble*, 2>& upgradedBubbles) {
    int deletionCount = 0, upgradeCount = 0, movementCount = 0, refillCount = 0;
    std::vector<Deletion> deletions;
    auto& changeCycles = latestResult_->getChangeCycles();

    if (changeCycles.empty()) {
        deletions = std::move(deletion(latestResult_->getUpgrades())); // perform first deletions
    } else {
        deletions = std::move(changeCycles.front().deletions()); // perform pre-computed deletions
        changeCycles.clear();
        performDeletion(deletions);
    }

    upgrade(latestResult_->getUpgrades(), bubbles, upgradedBubbles);

    for (int i = 0; !deletions.empty(); i++) { // let the cycle begin
        std::cout << "delete refill cycle " << i << std::endl;

        auto[movements, refills] = refill(); // drop and refill

        deletionCount += deletions.size();
        movementCount += movements.size();
        refillCount += refills.size();
        changeCycles.emplace_back(std::move(deletions), std::move(movements), std::move(refills)); // log changes for animation
        refreshCallback_(*this);

        const std::vector<Refill> upgradeDummy;
        deletions = std::move(deletion(upgradeDummy)); // perform deletions
    }

    // statistics
    std::cout << "--result--" << std::endl;
    std::cout << "swapped:  " << (latestResult_->getSwap() ? 2 : 0) << " bubbles" << std::endl;
    std::cout << "deleted:  " << deletionCount << " bubbles" << std::endl;
    std::cout << "upgraded: " << latestResult_->getUpgrades().size() << " bubbles" << std::endl;
    std::cout << "moved:    " << movementCount << " bubbles" << std::endl;
    std::cout << "refilled: " << refillCount << " bubbles" << std::endl;

    calcHasMovesLeft();
}

auto GameField::deletion(const std::vector<Refill>& upgrades) -> std::vector<Deletion> {
    std::vector<Deletion> deletions;
    std::vector<bool> deletionMap(bubbles_->bubbleCount());
    for (auto& change : upgrades) {
        int index = change.getSource().toIndex(size_);
        deletionMap[index] = true; // mark upgrade bubbles to not be deleted
    }
#if EXTENDED_ELIMINATION
    int deletionStart = 0;
#endif
    // Search single-colored rows / columns of length 3 or more to delete
    for (int xMul = 0, yMul = 1; xMul < 2; xMul++, yMul--) { // switch vertical/horizontal walking
        Position step(xMul, yMul);
        for (int y = 0; y < size_; y++) {
            Position current(yMul * y, xMul * y);
            Color rowColor = bubbles_->bubbleAt(current).getColor();
            int rowLength = 1;
            for (int x = 1; x < size_; x++) {
                current += step;
                Color color = bubbles_->bubbleAt(current).getColor();
                if (color == rowColor) {
                    rowLength++;
                    if (rowLength < MIN_ROW_LENGTH)
                        continue;
                    markForDeletion(deletions, deletionMap, current);
                    if (rowLength != MIN_ROW_LENGTH)
                        continue;
                    Position previous = current;
                    for (int i = 0; i < MIN_ROW_LENGTH - 1; i++) {
                        previous -= step;
                        markForDeletion(deletions, deletionMap, previous);
                    }
                    timeLeft_ += COMBINATION_TIME;
                } else {
                    rowColor = color;
                    rowLength = 1;
                }
            }
        }
#if EXTENDED_ELIMINATION
        // Search for adjacent bubbles with the same color to delete
        Position normalStep(yMul, xMul);
        for (int i = deletionStart; i < deletions.size(); i++) {
            Position root = deletions[i].getSource();
            const Color rootColor = bubbles_->bubbleAt(root).getColor();
            for (Position start : std::array<Position, 2>{ root - normalStep, root + normalStep }) {
                if (!start.inBounds(size_))
                    continue;
                std::queue<Position> toCheck;
                toCheck.push(start);
                do {
                    Position current = toCheck.front();
                    toCheck.pop();
                    if (!current.inBounds(size_) || deletionMap[current.toIndex(size_)] || bubbles_->bubbleAt(current).getColor() != rootColor)
                        continue;
                    markForDeletion(deletions, deletionMap, current);
                    for (Position direction : Position::DIRECTIONS) {
                        toCheck.push(current + direction);
                    }
                } while (!toCheck.empty());
            }
        }
        deletionStart = static_cast<int>(deletions.size());
#endif
    }
    for (auto& change : upgrades) {
        deletionMap[change.getSource().toIndex(size_)] = false;
    }
    performDeletion(deletions);
    return deletions;
}

inline void GameField::markForDeletion(std::vector<Deletion>& deletions, std::vector<bool>& deletionMap, Position position) {
    int index = position.toIndex(size_);
    if (!deletionMap[index]) {
        deletionMap[index] = true;
        deletions.emplace_back(position);
    }
}

void GameField::performDeletion(std::vector<Deletion>& deletions) {
    for (auto& change : deletions) {
        int index = change.getSource().toIndex(size_);
        Bubble* bubble = bubbles_->bubblePtrAt(index);
        if (bubble->hasSpecialAction())
            specialBubbleCount_--;
        delete bubble;
        bubbles_->bubblePtrAt(index) = nullptr;
        points_++;
    }
    std::cout << "deleted " << deletions.size() << " bubbles:" << std::endl;
    for (const auto& change : deletions) {
        std::cout << change << '\n';
    }
}

void GameField::upgrade(const std::vector<Refill>& upgrades, std::array<Bubble*, 2>& bubbles, const std::array<Bubble*, 2>& upgradedBubbles) {
    int upgradeCount = upgrades.size();
    std::cout << "upgraded " << upgradeCount << " bubbles:" << std::endl;
    for (int i = 0; i < 2; i++) {
        Bubble* upgradedBubble = upgradedBubbles[i];
        if (upgradedBubble == nullptr)
            continue;
        bubbles_->bubblePtrAt(upgradedBubble->getPosition()) = upgradedBubble;
        delete bubbles[i];
        points_++;
        std::cout << bubbles[i]->getPosition() << "->" << bubbles[i]->getColorAsLetter() << '\n';
    }
    if (upgradeCount != 0)
        refreshCallback_(*this);
}

auto GameField::refill() -> std::pair<std::vector<Movement>, std::vector<Refill>> {
    std::vector<Movement> movements;
    std::vector<Refill> refills;
    for (int rootIndex = size_ * (size_ - 1), x = 0; x < size_; x++, rootIndex++) {
        int space = 0;
        // gravity
        for (int index = rootIndex, y = size_ - 1; y >= 0; y--, index -= size_) {
            if (!bubbles_->isBubbleAt(index)) {
                space++;
            } else if (space > 0) {
                Position to(x, y + space);
                movements.emplace_back(Position(x, y), to);
                (bubbles_->bubblePtrAt(index + space * size_) = bubbles_->bubblePtrAt(index))->setPosition(to);
            }
        }
        // refill
        for (int index = x, y = 0; y < space; y++, index += size_) {
            Position current(x, y);
            Bubble* bubble = createBubble(current);
            refills.emplace_back(Position(x, y - space), current, bubble->getColor());
            bubbles_->bubblePtrAt(index) = bubble;
        }
    }
    std::cout << "moved " << movements.size() << " bubbles:" << std::endl;
    for (const auto& change : movements) {
        std::cout << change << '\n';
    }
    std::cout << "refilled " << refills.size() << " bubbles:" << std::endl;
    for (const auto& change : refills) {
        std::cout << change << '\n';
    }
    return { movements, refills };
}

inline auto GameField::createBubble(Position position) -> Bubble* {
    return new Bubble(position, static_cast<Color>(gen_() % DEFAULT_COLOR_SIZE));
}

void GameField::calcHasMovesLeft() {
    if (specialBubbleCount_) {
        hasMovesLeft_ = true;
        std::cout << "Possible move: Special Bubble" << std::endl;
        return;
    }
    std::array<Position, 2> positions;
    Position& p = positions[0];
    std::array<int, 2> indices;
    for (int i = 0; i < 2; i++) { // up-down or right-left swap
        Position step = Position::DIRECTIONS[i];
        for (p.y(0); p.y() < size_ - step.y(); p.y(p.y() + 1)) {
            indices[0] = p.y() * size_;
            for (p.x(0); p.x() < size_ - step.x(); p.x(p.x() + 1), indices[0]++) {
                positions[1] = positions[0] + step;
                indices[1] = indices[0] + (i ? 1 : size_);
                auto[allowed, rowLengths, maxRowLengths, bubbles] = checkSwap(positions, indices, true);
                if (allowed) {
                    hasMovesLeft_ = true;
                    std::cout << "Possible move: " << positions[0] << "<->" << positions[1] << std::endl;
                    return;
                }
            }
        }
    }
    std::cout << "Game Over" << std::endl;
    hasMovesLeft_ = false;
}
