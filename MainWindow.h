#ifndef CANDYCRUSH_MAINWINDOW_H
#define CANDYCRUSH_MAINWINDOW_H


class MainWindow;

#include "GameField.h"
#include "ColoredWidget.h"
#include "Button.h"
#include <QtWidgets/QMainWindow>
#include <ui_MainWindow.h>
#include <qgridlayout.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qtimer.h>
#include <QInputDialog>
#include <QMessageBox>
#include <memory>
#include <vector>
#include <list>
#include <optional>
#include <iostream>

/// The main window
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QApplication* app);
    /// \return the padding between buttons
    auto padding() const -> int;
    /// \return the size of each button
    auto buttonSize() const -> int;
    /// \return the offset of each button
    auto offset() const -> int;
private:
    static const int DEFAULT_SIZE = 5;
    static const int BORDER_SIZE = 50;
    static const int BUBBLE_SIZE = 500;
    /// Initializes the gui
    void init();
    /// Initializes the menu bar
    void createMenu();
    /// Performs a click
    /// \param position where the click happened
    void handleButtonClick(Position position);
    /// Second-tick handler
    void handleTimer();
    /// Performs game reset
    void handleResetGame();
    /// Performs game resize
    /// Currently only working when game is started :(
    bool handleResizeGame();
    /// Quits the game
    void handleQuitGame();
    /// Updated the status bar
    void showMessage();
    /// Display swapped buttons
    /// \param clickResult contains changes to the game field
    void swapButtons(std::shared_ptr<const ClickResult> clickResult);
    /// Display upgraded buttons
    /// \param clickResult contains changes to the game field
    void upgradeButtons(std::shared_ptr<const ClickResult> clickResult);
    /// Display deletions and refills
    /// \param clickResult contains changes to the game field
    /// \param index the index of the changeCycle
    void processChangeCycles(std::shared_ptr<const ClickResult> clickResult, int index);
    /// Convenient method to get the button at a position
    /// \param position where
    /// \return the requested bubble reference
    auto buttonAt(Position position) -> std::unique_ptr<Button>&;
    /// Applies the color as given by gameField to the button at this position
    /// \param position where
    void setColor(Position position);
    /// Applies a color to the button at this position
    /// \param position where
    /// \param color new color
    void setColor(Position position, Color color);
    /// Debug output method to use as callback
    /// \param gameField GameField to print
    void showField(GameField& gameField);
    /// Currently running game
    GameField gameField_;
    /// Padding between buttons
    int padding_;
    /// Size of a button
    int buttonSize_;
    /// Offset of a button
    int offset_;
    /// Selection text
    QString selection_ = "-:-";
    /// Status text
    QString message_ = "";
    /// UI
    Ui::BubbleTwistGUIClass ui_;
    /// Application (used to stop the game)
    QApplication* app_;
    /// Central widget for background color
    std::unique_ptr<QWidget> centralWidget_;
    /// Holds all buttons sorted by their index
    std::vector<std::unique_ptr<Button>> buttonStorage_;
    /// Used to make buttons round
    std::unique_ptr<QRegion> bubbleRegion_;
    /// Menu bar
    QMenu* gameMenu_;
    /// Option for new game
    std::unique_ptr<QAction> actionResetGame_;
    /// Option to change size (currently not working)
    std::unique_ptr<QAction> actionResizeGame_;
    /// Option to quit game
    std::unique_ptr<QAction> actionQuitGame_;
    /// Timer for the count down
    std::unique_ptr<QTimer> mainTimer_;
    /// Timers for animation scheduling
    std::list<std::unique_ptr<QTimer>> timers_;
    /// Locks game while calculating a move and doing animations
    bool busy_ = false;
};


#endif //CANDYCRUSH_MAINWINDOW_H
