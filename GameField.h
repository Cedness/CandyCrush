//
// Created by ced on 28.05.21.
//

#ifndef CANDYCRUSH_GAMEFIELD_H
#define CANDYCRUSH_GAMEFIELD_H


#define MIN_ROW_LENGTH 3
#define MAX_ROW_LENGTH 5
#define COMBINATION_TIME 1.5

#define EXTENDED_ELIMINATION true
#define REVERSE_CLICK_COLOR_BUBBLE false

class GameField;

#include "Deletion.h"
#include "Movement.h"
#include "Refill.h"
#include "Bubble.h"
#include "Position.h"
#include "PlacementArray.h"
#include "ClickResult.h"
#include "BubbleContainer.h"
#include <vector>
#include <memory>
#include <functional>
#include <tuple>
#include <utility>
#include <random>
#include <chrono>

/// Represents a candy crush game
class GameField {
    constexpr static const double START_TIME = 15.0;
public:
    /// Creates a game
    /// \param size the number of bubbles per row
    GameField(int size);
    /// Creates a game
    /// \param size the number of bubbles per row
    /// \param refreshCallback a callback function to draw the field onto the debug console
    GameField(int size, std::function<void (GameField& g)> refreshCallback);
    /// Move assignment operator for the size change
    GameField& operator=(GameField&& gameField) noexcept ;
private:
    /// Recreates the game until at least one valid move is available
    void initUntilDone();
public:
    /// Resets the game
    void reset();
    /// \return the number of bubbles in one row or column
    auto size() const -> int;
    /// Only useful to the debug-output which is called while processing
    /// \param position where
    /// \return @true if there is a bubble at the position
    auto isBubbleAt(Position position) -> bool;
    /// \param position position whose bubble shall be obtained
    /// \return Reference to the requested bubble
    auto bubbleAt(Position position) -> const Bubble&;
    /// @see @code bubbleAt(Position)
    auto operator[](Position position) -> const Bubble&;
    /// This method should be called every second for the countdown to work
    void tick();
    /// \return the time left for the user to react
    auto getTimeLeft() const -> int;
    /// \return @true if the game is over
    auto isGameOver() const -> bool;
    /// \return @true if there are moves left for the player
    auto hasMovesLeft() const -> bool;
    /// \return the result of the latest click
    auto getLatestResult() const -> std::shared_ptr<const ClickResult>;
    /// \return the points
    auto getPoints() const -> int;
    /// Perform a click on a bubble
    /// \param position of the bubble
    /// \return the result, containing all changes
    auto clickedAt(Position position) -> std::shared_ptr<const ClickResult>;
private:
    /// Perform a click on a bubble, part 2
    /// \param position of the bubble
    void performClickAction(Position position);
    /// Performs a special action from a special bubble
    /// \param specialBubble the bubble whose action is to perform
    /// \param otherBubble nullptr if not required
    void specialAction(Bubble& specialBubble, Bubble* otherBubble);
    /// Performs a swap and its aftereffects
    /// \param position1 where one bubble is
    /// \param position2 where the other bubble is
    void swap(Position position1, Position position2);
    /// Attempts to do a swap and checks requirements
    auto checkUserSwap(std::array<Position, 2>& positions, std::array<int, 2>& indices) -> std::tuple<std::array<std::array<int, 2>, 2>, std::array<int, 2>, std::array<Bubble*, 2>>;
    /// Checks the "3 in a row"-requirement for the swap
    auto checkSwap(std::array<Position, 2>& positions, std::array<int, 2>& indices, bool undo) -> std::tuple<bool, std::array<std::array<int, 2>, 2>, std::array<int, 2>, std::array<Bubble*, 2>>;
    /// Creates the special bubbles required by this move
    auto createUpgradedBubbles(std::vector<Refill>& upgrades, std::array<Position, 2>& positions, std::array<Bubble*, 2>& bubbles, std::array<std::array<int, 2>, 2>& rowLengths, std::array<int, 2>& maxRowLengths) -> std::array<Bubble*, 2>;
    /// Start the delete and refill cycle
    void deleteAndRefill(std::array<Bubble*, 2>& bubbles, const std::array<Bubble*, 2>& upgradedBubbles);
    /// Start the delete and refill cycle
    void deleteAndRefill();
    /// Calculates deletions for one cycle by scanning for rows of 3 or more
    auto deletion(const std::vector<Refill>& upgrades) -> std::vector<Deletion>;
    /// Marks a bubble for deletion
    inline void markForDeletion(std::vector<Deletion>& deletions, std::vector<bool>& deletionMap, Position position);
    /// Performs the calculated deletions
    inline void performDeletion(std::vector<Deletion>& deletions);
    /// Place the special bubbles onto the field
    void upgrade(const std::vector<Refill>& upgrades, std::array<Bubble*, 2>& bubbles, const std::array<Bubble*, 2>& upgradedBubbles);
    /// Let gravity do its thing and refill with new bubbles
    auto refill() -> std::pair<std::vector<Movement>, std::vector<Refill>>;
    /// Creates a random-colored bubble
    /// \param position initial Position of the bubble
    /// \return pointer to the bubble
    inline auto createBubble(Position position) -> Bubble*;
    /// Calculates whether the user has valid moves left to do and saves the result hasMovesLeft_
    void calcHasMovesLeft();
    /// Time-based random number generator
    std::mt19937 gen_{ static_cast<unsigned int>(std::chrono::steady_clock::now().time_since_epoch().count()) };
    /// Number of bubbles in one row or column
    int size_;
    /// Stores the bubbles
    std::unique_ptr<BubbleContainer> bubbles_;
    /// Time left for the user to react in seconds
    double timeLeft_;
    /// Count of special bubbles currently in the game
    int specialBubbleCount_ = 0;
    /// Last result of calcHasMovesLeft()
    bool hasMovesLeft_ = true;
    /// Result of the latest click or the creation of the board
    std::shared_ptr<ClickResult> latestResult_{ std::make_shared<ClickResult>() };
    /// Points the user has earned
    int points_ = 0;
    /// Callback to draw the field onto the debug console
    std::function<void (GameField& g)> refreshCallback_;
    /// Remembers the first clicked position, so that the second clicked one can be swapped with this one
    Position selection_ = Position::INVALID;
};


#endif //CANDYCRUSH_GAMEFIELD_H
