//
// Created by Ced on 26/07/2021.
//

#ifndef CANDYCRUSH_REFILL_H
#define CANDYCRUSH_REFILL_H


#include "Change.h"

class Refill : public Change {
public:
    Refill(Position from, Position to, Color color);
    auto getSource() const -> Position override;
    auto getTarget() const -> Position override;
    auto getColor() const -> Color override;
private:
    Position from_;
    Position to_;
    Color color_;
};


#endif //CANDYCRUSH_REFILL_H
