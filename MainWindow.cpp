#include "MainWindow.h"
#include "CCException.h"

MainWindow::MainWindow(QApplication* app)
: gameField_(DEFAULT_SIZE, [this](GameField& g) { showField(g); }), QMainWindow(nullptr), app_(app)
{
    ui_.setupUi(this);
    setFixedSize(720, 720);

    centralWidget_ = std::make_unique<ColoredWidget>(QColor(255, 228, 181), this);
    createMenu();
    setCentralWidget(centralWidget_.get());

    mainTimer_ = std::make_unique<QTimer>();
    mainTimer_->setSingleShot(false);
    mainTimer_->setInterval(1000);
    connect(mainTimer_.get(), &QTimer::timeout, [this](){
        handleTimer();
    });

    if (!handleResizeGame())
        throw CCException("Cancelled");
}

auto MainWindow::padding() const -> int {
    return padding_;
}

auto MainWindow::buttonSize() const -> int {
    return buttonSize_;
}

auto MainWindow::offset() const -> int {
    return offset_;
}

void MainWindow::init() {
    int size = gameField_.size();

    padding_ = (width() - 2 * BORDER_SIZE) / size;
    buttonSize_ = BUBBLE_SIZE / size;
    offset_ = padding_ / 2 - buttonSize_ / 2 + BORDER_SIZE;

    bubbleRegion_ = std::make_unique<QRegion>(QRect(3, 3, buttonSize_ - 6, buttonSize_ - 6), QRegion::Ellipse);

    //buttonStorage_.reserve(size * size);
    for (int indexRows = 0; indexRows < size; indexRows++)
    {
        for (int indexColumns = 0; indexColumns < size; indexColumns++)
        {
            auto& buttonData = buttonStorage_.emplace_back(new Button(this));
            auto* button = buttonData->button();

            buttonData->setPosition(Position(indexColumns, indexRows));


            const Bubble& bubble = gameField_.bubbleAt(Position(indexColumns, indexRows));
            buttonData->setColor(bubble.getColor());

            button->setMask(*bubbleRegion_);

            connect(button, &QPushButton::clicked, [this, button]()
            {
                for (int index = 0, y = 0; y < gameField_.size(); y++) {
                    for (int x = 0; x < gameField_.size(); x++, index++) {
                        if (button == buttonStorage_[index]->button()) {
                            handleButtonClick(Position(x, y));
                            return;
                        }
                    }
                }
                std::cout << "Button not found" << std::endl;
            });
        }
    }
    mainTimer_->start();
    showMessage();
}


void MainWindow::createMenu()
{
    gameMenu_ = menuBar()->addMenu("Spiel");

    actionResetGame_ = std::make_unique<QAction>("&Neues Spiel", this);
    //actionResizeGame_ = std::make_unique<QAction>("&Größe ändern", this);
    actionQuitGame_ = std::make_unique<QAction>("&Beenden", this);

    gameMenu_->addAction(actionResetGame_.get());
    gameMenu_->addAction(actionResizeGame_.get());
    gameMenu_->addAction(actionQuitGame_.get());

    connect(actionResetGame_.get(), &QAction::triggered, this, [this]()
    {
        handleResetGame();
    });
    connect(actionResizeGame_.get(), &QAction::triggered, this, [this]()
    {
        handleResizeGame();
    });
    connect(actionQuitGame_.get(), &QAction::triggered, this, [this]()
    {
        handleQuitGame();
    });
}

void MainWindow::handleButtonClick(Position position)
{
    try
    {
        if (busy_) {
            message_ = "Wait";
        } else {
            busy_ = true;
            timers_.clear();
            auto clickResult = gameField_.clickedAt(position);
            selection_ = clickResult->isSelected() ? QString(static_cast<char>('A' + position.x())) + ":" + QString::number(position.y()) : "-:-";

            if (clickResult->hasGameFieldChanged()) {
                swapButtons(clickResult);
                message_ = "+" + QString::number(clickResult->getAddedPoints()) + "P  +" + QString::number(clickResult->getAddedTime()) + "s";
            }
            if (timers_.empty())
                busy_ = false;
        }
    }
    catch (CCException& ex)
    {
        selection_ = "-:-";
        message_ = ex.what();
        std::cout << ex.what() << std::endl;
    }
    showMessage();
}

void MainWindow::handleTimer()
{
    gameField_.tick();
    showMessage();
}

void MainWindow::handleResetGame()
{
    gameField_.reset();
    for (Position current; current.y() < gameField_.size(); current.y(current.y() + 1)) {
        for (current.x(0); current.x() < gameField_.size(); current.x(current.x() + 1)) {
            setColor(current);
        }
    }
    mainTimer_->stop();
    for (auto& aTimer : timers_) {
        if (aTimer->isActive())
            aTimer->stop();
    }
    timers_.clear();
    busy_ = false;
    mainTimer_->start();
    message_ = "";
    showMessage();
}

bool MainWindow::handleResizeGame() {
    bool ok;
    int newSize = QInputDialog::getInt(this, "Größe ändern", "Neue Größe", gameField_.size(), 3, 100, 1, &ok);
    if (!ok)
        return false;
    mainTimer_->stop();
    for (auto& aTimer : timers_) {
        if (aTimer->isActive())
            aTimer->stop();
    }
    timers_.clear();
    busy_ = false;
    selection_ = "-:-";
    message_ = "";

    buttonStorage_.clear();

    gameField_ = GameField(newSize, [this](GameField& g) { showField(g); });
    init();

    return true;
}

void MainWindow::handleQuitGame()
{
    QApplication::quit();
}

void MainWindow::showMessage() {
    if (gameField_.isGameOver())
        message_ = "Game Over";
    statusBar()->showMessage("Zeit: " + QString::number(gameField_.getTimeLeft()) +
                             "   |   Punkte: " + QString::number(gameField_.getPoints()) +
                             "   |   Position: " + selection_ +
                             "   |   " + message_, -1);
}

void MainWindow::swapButtons(std::shared_ptr<const ClickResult> clickResult)
{
    busy_ = true;
    auto& swap = clickResult->getSwap();
    if (swap) {
        Position p1 = swap->getSource();
        Position p2 = swap->getTarget();

        auto& b1 = buttonAt(p1);
        auto& b2 = buttonAt(p2);
        b1->wooooshPosition(p2);
        b2->wooooshPosition(p1);
        b1.swap(b2);
        auto& upgradeTimer = timers_.emplace_back(new QTimer());
        upgradeTimer->setSingleShot(true);
        upgradeTimer->setInterval(250);
        connect(upgradeTimer.get(), &QTimer::timeout, [this, clickResult](){
            upgradeButtons(clickResult);
        });
        upgradeTimer->start();
    } else {
        upgradeButtons(clickResult);
    }
}

void MainWindow::upgradeButtons(std::shared_ptr<const ClickResult> clickResult)
{
    for (const auto& change : clickResult->getUpgrades())
    {
        setColor(change.getSource(), change.getColor());
    }
    processChangeCycles(clickResult, 0);
}

void MainWindow::processChangeCycles(std::shared_ptr<const ClickResult> clickResult, int index)
{
    auto& changeCycles = clickResult->getChangeCycles();
    if (index >= changeCycles.size()) {
        busy_ = false;
        return;
    }
    auto& changeCycle = clickResult->getChangeCycles()[index];

    std::vector<Button*> deletedWidgets;
    deletedWidgets.reserve(changeCycle.deletions().size());
    for (const auto& change : changeCycle.deletions())
    {
        Position position = change.getSource();
        auto& button = buttonAt(position);
        deletedWidgets.push_back(button.get());
        button.release();
        button = nullptr;
    }
    for (const auto& change : changeCycle.movements())
    {
        Position positionSource = change.getSource();
        Position positionTarget = change.getTarget();
        auto& b1 = buttonAt(positionSource);
        auto& b2 = buttonAt(positionTarget);
        b1->wooooshPosition(positionTarget);
        b1.swap(b2);
    }
    for (const auto& change : changeCycle.refills())
    {
        Position source = change.getSource();
        Position target = change.getTarget();
        auto& button = buttonAt(target);
        button = std::unique_ptr<Button>(deletedWidgets.back());
        deletedWidgets.pop_back();
        button->setColor(change.getColor());
        button->setPosition(source);
        button->wooooshPosition(target);
    }

    auto& changeCycleTimer = timers_.emplace_back(new QTimer());
    changeCycleTimer->setSingleShot(true);
    changeCycleTimer->setInterval(250);
    connect(changeCycleTimer.get(), &QTimer::timeout, [this, clickResult, index](){
        processChangeCycles(clickResult, index + 1);
    });
    changeCycleTimer->start();
}

auto MainWindow::buttonAt(Position position) -> std::unique_ptr<Button>& {
    return buttonStorage_[position.toIndex(gameField_.size())];
}

void MainWindow::setColor(Position position) {
    setColor(position, gameField_.bubbleAt(position).getColor());
}

void MainWindow::setColor(Position position, Color color) {
    buttonAt(position)->setColor(color);
}

void MainWindow::showField(GameField& gameField) {
    int gameFieldSize = gameField.size();
    std::cout << gameFieldSize << "\n";
    for (int y = 0; y < gameFieldSize; y++) {
        if (y != 0)
        {
            for (int index = 0; index < (2 * ((gameFieldSize * 2))) - 1; index++)
            {
                if ((index % 4) == 3)
                    std::cout << "+";
                else
                    std::cout << "-";
            }
        }
        std::cout << std::endl;
        for (int x = 0; x < gameFieldSize; x++) {
            std::cout << " ";
            if (x != 0)
                std::cout << "| ";
            Position position(x, y);
            std::cout << (gameField.isBubbleAt(position) ? gameField.bubbleAt(position).getColorAsLetter() : ' ');
        }
        std::cout << std::endl;
    }
}
