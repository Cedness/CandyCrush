//
// Created by Ced on 16/07/2021.
//

#ifndef CANDYCRUSH_CHANGECYCLE_H
#define CANDYCRUSH_CHANGECYCLE_H


class ChangeCycle;

#include "Deletion.h"
#include "Movement.h"
#include "Refill.h"
#include <vector>

/// Represents one cycle of deletion, movement through gravity and refills
class ChangeCycle {
public:
    //ChangeCycle(std::vector<Change> deletions, std::vector<Change> movements, std::vector<Change> refills_);
    ChangeCycle(std::vector<Deletion>&& deletions, std::vector<Movement>&& movements, std::vector<Refill>&& refills_);
    auto hasGameFieldChanged() const -> bool;
    auto deletions() const -> const std::vector<Deletion>&;
    auto movements() const -> const std::vector<Movement>&;
    auto refills() const -> const std::vector<Refill>&;
    auto deletions() -> std::vector<Deletion>&;
    auto movements() -> std::vector<Movement>&;
    auto refills() -> std::vector<Refill>&;
private:
    /// Deletions which happened during this cycle
    std::vector<Deletion> deletions_;
    /// Movements which happened during this cycle
    std::vector<Movement> movements_;
    /// Refills which happened during this cycle
    std::vector<Refill> refills_;
};


#endif //CANDYCRUSH_CHANGECYCLE_H
