//
// Created by Ced on 16/07/2021.
//

#ifndef CANDYCRUSH_BUBBLECONTAINER_H
#define CANDYCRUSH_BUBBLECONTAINER_H


class BubbleContainer;

#include "Bubble.h"
#include "Position.h"
#include <vector>
#include <functional>

/// Wrapper for a bubble-vector
class BubbleContainer {
public:
    BubbleContainer(int size, std::function<Bubble*(Position)> bubbleGenerator);
    ~BubbleContainer();
    /// Refill vector with random bubbles
    void refill(std::function<Bubble*(Position)> bubbleGenerator);
    /// \return count of ALL BUBBLES, not of bubbles per row
    auto bubbleCount() const -> int;
    auto isBubbleAt(int index) -> bool;
    auto isBubbleAt(Position position) -> bool;
    auto bubblePtrAt(int index) -> Bubble*&;
    auto bubblePtrAt(Position position) -> Bubble*&;
    auto bubbleAt(int index) -> Bubble&;
    auto operator[](int index) -> Bubble&;
    auto bubbleAt(Position position) -> Bubble&;
    auto operator[](Position position) -> Bubble&;
    /// Swaps two bubbles
    void doSwap(int index1, int index2);
private:
    /// Fills the vector with random colored bubbles, call only when empty
    void fill(std::function<Bubble*(Position)> bubbleGenerator);
    /// Guess what this is for
    void clear();
    /// Count of BUBBLES PER ROW, not of the vector
    int size_;
    /// The bubbles
    std::vector<Bubble*> bubbles_;
};


#endif //CANDYCRUSH_BUBBLECONTAINER_H
