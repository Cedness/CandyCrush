//
// Created by Ced on 17/07/2021.
//

#ifndef CANDYCRUSH_ROWCOLUMNBUBBLE_H
#define CANDYCRUSH_ROWCOLUMNBUBBLE_H


#include "SpecialBubble.h"

class RowColumnBubble : public SpecialBubble {
public:
    RowColumnBubble(Position position, bool columnMode);
protected:
    auto performFirstClickAction(GameField& gameField) -> std::pair<double, std::vector<Deletion>> override;
private:
    bool columnMode_;
};


#endif //CANDYCRUSH_ROWCOLUMNBUBBLE_H
