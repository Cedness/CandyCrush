//
// Created by Ced on 26/07/2021.
//

#ifndef CANDYCRUSH_DELETION_H
#define CANDYCRUSH_DELETION_H


#include "Change.h"

class Deletion : public Change {
public:
    Deletion(Position position);
    auto getSource() const -> Position override;
private:
    Position position_;
};


#endif //CANDYCRUSH_DELETION_H
