//
// Created by Ced on 18/06/2021.
//

#ifndef CANDYCRUSH_CLICKRESULT_H
#define CANDYCRUSH_CLICKRESULT_H


class ClickResult;

#include "ChangeCycle.h"
#include "Movement.h"
#include "Refill.h"
#include <vector>
#include <optional>

// Logs changes to the game field in chronological order to allow later animation
class ClickResult {
public:
    ClickResult();
    auto isSelected() const -> bool;
    auto isSelected() -> bool&;
    auto getAddedPoints() const -> int;
    auto getAddedPoints() -> int&;
    auto getAddedTime() const -> double;
    auto getAddedTime() -> double&;
    auto hasGameFieldChanged() const -> bool;
    auto getSwap() const -> const std::optional<Movement>&;
    auto getSwap() -> std::optional<Movement>&;
    auto getUpgrades() const -> const std::vector<Refill>&;
    auto getUpgrades() -> std::vector<Refill>&;
    auto getChangeCycles() const -> const std::vector<ChangeCycle>&;
    auto getChangeCycles() -> std::vector<ChangeCycle>&;
private:
    bool selected_ = true;
    int addedPoints_ = 0;
    double addedTime_ = 0.0;
    /// Optional info about a swap, which is only present if this click resulted in a swap
    std::optional<Movement> swap_;
    /// Upgraded bubbles, only filled if upgrades happened at all
    std::vector<Refill> upgrades_;
    /// Cycles of deletions, movements and refills that happened because of the click
    std::vector<ChangeCycle> changeCycles_;
};


#endif //CANDYCRUSH_CLICKRESULT_H
