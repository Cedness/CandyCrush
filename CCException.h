//
// Created by Ced on 25/06/2021.
//

#ifndef CANDYCRUSH_CCEXCEPTION_H
#define CANDYCRUSH_CCEXCEPTION_H


#include <stdexcept>

class CCException : public std::logic_error {
public:
    explicit CCException(const std::string& msg);
};


#endif //CANDYCRUSH_CCEXCEPTION_H
