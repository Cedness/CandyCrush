//
// Created by ced on 28.05.21.
//

#ifndef CANDYCRUSH_USERINTERFACE_H
#define CANDYCRUSH_USERINTERFACE_H


#include "GameField.h"

/// Old console interface
/// Should still be working, but no guarantee
class UserInterface {
public:
    UserInterface(int size);
    void runGame();
    void showField(GameField& gameField);
    auto getUserInputBubble() -> Position;
private:
    GameField gameField_;
};


#endif //CANDYCRUSH_USERINTERFACE_H
