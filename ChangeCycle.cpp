//
// Created by Ced on 16/07/2021.
//

#include "ChangeCycle.h"

#include <utility>

//ChangeCycle::ChangeCycle(std::vector<Change> deletions, std::vector<Change> movements, std::vector<Change> refills_)
//        : deletions_(std::move(deletions)), movements_(std::move(movements)), refills_(std::move(refills_)) {}

ChangeCycle::ChangeCycle(std::vector<Deletion>&& deletions, std::vector<Movement>&& movements, std::vector<Refill>&& refills_)
        : deletions_(std::move(deletions)), movements_(std::move(movements)), refills_(std::move(refills_)) {}

auto ChangeCycle::hasGameFieldChanged() const -> bool {
    return !(deletions_.empty() && movements_.empty() && refills_.empty());
}

auto ChangeCycle::deletions() const -> const std::vector<Deletion>& {
    return deletions_;
}

auto ChangeCycle::movements() const -> const std::vector<Movement>& {
    return movements_;
}

auto ChangeCycle::refills() const -> const std::vector<Refill>& {
    return refills_;
}

auto ChangeCycle::deletions() -> std::vector<Deletion>& {
    return deletions_;
}

auto ChangeCycle::movements() -> std::vector<Movement>& {
    return movements_;
}

auto ChangeCycle::refills() -> std::vector<Refill>& {
    return refills_;
}
