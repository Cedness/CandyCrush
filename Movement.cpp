//
// Created by Ced on 26/07/2021.
//

#include "Movement.h"

Movement::Movement(Position from, Position to)
        : from_(from), to_(to) {}

Position Movement::getSource() const {
    return from_;
}

Position Movement::getTarget() const {
    return to_;
}
