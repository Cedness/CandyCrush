//
// Created by ced on 28.05.21.
//

#include "Bubble.h"

Bubble::Bubble(Position position, Color color)
        : position_(position), color_(color) {}

Bubble::~Bubble() = default;

auto Bubble::getPosition() const -> Position {
    return position_;
}

void Bubble::setPosition(Position position) {
    position_ = position;
}

auto Bubble::getColor() const -> Color {
    return color_;
}

auto Bubble::getColorAsLetter() const -> char {
    return colorAsLetter(color_);
}

auto Bubble::hasSpecialAction() const -> bool {
    return false;
}

auto Bubble::isTwoStepAction() const -> bool {
    return false;
}

auto Bubble::performFirstClickAction(GameField& gameField) -> std::pair<double, std::vector<Deletion>> {
    return { 0, std::vector<Deletion>() };
}

auto Bubble::performSecondClickAction(GameField& gameField, Bubble& firstClick) -> std::pair<double, std::vector<Deletion>> {
    return  { 0, std::vector<Deletion>() };
}
