//
// Created by Ced on 18/06/2021.
//

#include "ClickResult.h"

ClickResult::ClickResult() = default;

auto ClickResult::isSelected() const -> bool {
    return selected_;
}

auto ClickResult::isSelected() -> bool& {
    return selected_;
}

auto ClickResult::getAddedPoints() const -> int {
    return addedPoints_;
}

auto ClickResult::getAddedPoints() -> int& {
    return addedPoints_;
}

auto ClickResult::getAddedTime() const -> double {
    return addedTime_;
}

auto ClickResult::getAddedTime() -> double& {
    return addedTime_;
}

auto ClickResult::hasGameFieldChanged() const -> bool {
    if (swap_ || !upgrades_.empty())
        return true;
    for (const auto& changeCycle : changeCycles_) {
        if (changeCycle.hasGameFieldChanged())
            return true;
    }
    return false;
}

auto ClickResult::getSwap() const -> const std::optional<Movement>& {
    return swap_;
}

auto ClickResult::getSwap() -> std::optional<Movement>& {
    return swap_;
}

auto ClickResult::getUpgrades() const -> const std::vector<Refill>& {
    return upgrades_;
}

auto ClickResult::getUpgrades() -> std::vector<Refill>& {
    return upgrades_;
}

auto ClickResult::getChangeCycles() const -> const std::vector<ChangeCycle>& {
    return changeCycles_;
}

auto ClickResult::getChangeCycles() -> std::vector<ChangeCycle>& {
    return changeCycles_;
}
