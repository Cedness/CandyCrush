//
// Created by Ced on 26/07/2021.
//

#include "Color.h"

auto colorAsLetter(Color color) -> char {
    switch (color) {
        case BLUE:       return 'B';
        case GREEN:      return 'G';
        case RED:        return 'R';
        case PINK:       return 'P';
        case YELLOW:     return 'Y';
        case LIGHT_GRAY: return '.';
        case DARK_GRAY:  return '+';
        case BLACK:      return '*';
        default:         return '?';
    }
}
