//
// Created by Ced on 17/07/2021.
//

#ifndef CANDYCRUSH_COLORBUBBLE_H
#define CANDYCRUSH_COLORBUBBLE_H


#include "SpecialBubble.h"

class ColorBubble : public SpecialBubble {
public:
    ColorBubble(Position position);
protected:
    auto isTwoStepAction() const -> bool override;
    auto performSecondClickAction(GameField& gameField, Bubble& firstClick) -> std::pair<double, std::vector<Deletion>> override;
};


#endif //CANDYCRUSH_COLORBUBBLE_H
