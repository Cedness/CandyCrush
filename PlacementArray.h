//
// Created by ced on 28.05.21.
//

#ifndef CANDYCRUSH_PLACEMENTARRAY_H
#define CANDYCRUSH_PLACEMENTARRAY_H


#include <cstddef>
#include <malloc.h>
#include <utility>
#include <stdexcept>

/// Just-for-fun class which was our first attempt to hold bubbles on the stack
/// Now they are in a vector on the heap, but managed through unique_ptr's, which allows for easier moving but less efficiency while creating them
template<typename T>
class PlacementArray {
public:
    explicit PlacementArray(size_t size);
    ~PlacementArray();
    T* raw();
    operator T*();
    template<typename... Args>
    T& emplace(size_t index, Args&&... args);
    const T& at(size_t index) const;
    T& at(size_t index);
    const T& operator[](size_t index) const;
    T& operator[](size_t index);
    void move(size_t indexFrom, size_t indexTo);
    void swap(size_t indexA, size_t indexB);
    size_t size() const;
    bool empty() const;
    operator bool() const;
private:
    T* const values_;
    const size_t size_;
};

template<typename T>
PlacementArray<T>::PlacementArray(size_t size)
        : values_((T*) new char[size * sizeof(T)]), size_(size) {}

template<typename T>
PlacementArray<T>::~PlacementArray() {
    for (size_t i = 0; i < size_; i++) {
        values_[i].~T();
    }
    delete[] (char*) values_;
}

template<typename T>
T* PlacementArray<T>::raw() {
    return values_;
}

template<typename T>
PlacementArray<T>::operator T*() {
    return values_;
}

template<typename T>
template<typename... Args>
T& PlacementArray<T>::emplace(size_t index, Args&&... args) {
    return *(new (values_ + index) T(std::forward<Args>(args)...));
}

template<typename T>
const T& PlacementArray<T>::operator[](size_t index) const {
    return at(index);
}

template<typename T>
T& PlacementArray<T>::operator[](size_t index) {
    return at(index);
}

template<typename T>
const T& PlacementArray<T>::at(size_t index) const {
    if (index < 0 || index >= size_)
        throw std::out_of_range("Index out of range");
    return values_[index];
}

template<typename T>
T& PlacementArray<T>::at(size_t index) {
    if (index < 0 || index >= size_)
        throw std::out_of_range("Index out of range");
    return values_[index];
}

template<typename T>
void PlacementArray<T>::move(size_t indexFrom, size_t indexTo) {
    at(indexTo).~T();
    at(indexTo) = std::move(at(indexFrom));
}

template<typename T>
void PlacementArray<T>::swap(size_t indexA, size_t indexB) {
    T temp = std::move(at(indexA));
    at(indexA) = std::move(at(indexB));
    at(indexB) = std::move(temp);
}

template<typename T>
size_t PlacementArray<T>::size() const {
    return size_;
}

template<typename T>
bool PlacementArray<T>::empty() const {
    return size_ == 0u;
}

template<typename T>
PlacementArray<T>::operator bool() const {
    return !empty();
}


#endif //CANDYCRUSH_PLACEMENTARRAY_H
