//
// Created by Ced on 16/07/2021.
//

#include "SquareBubble.h"
#include <iostream>

SquareBubble::SquareBubble(Position position) : SpecialBubble(position, LIGHT_GRAY) {}

auto SquareBubble::performFirstClickAction(GameField& gameField) -> std::pair<double, std::vector<Deletion>> {
    std::vector<Deletion> deletions;
    deletions.reserve(4);
    Position current;
    for (current.y(getPosition().y() - 1); current.y() <= getPosition().y() + 1; current.y(current.y() + 1)) {
        for (current.x(getPosition().x() - 1); current.x() <= getPosition().x() + 1; current.x(current.x() + 1)) {
            if (!current.inBounds(gameField.size()))
                continue;
            deletions.emplace_back(current);
        }
    }
    return { 3, deletions };
}
