//
// Created by ced on 28.05.21.
//

#include "UserInterface.h"
#include "CCException.h"
#include <iostream>


UserInterface::UserInterface(int size)
        : gameField_(GameField(size, [this](GameField& g) { showField(g);})) {}

void UserInterface::runGame()
{
    while (true) {
        try {
            Position p = UserInterface::getUserInputBubble();
            gameField_.clickedAt(p);
        } catch (CCException& ex) {
            std::cout << ex.what() << std::endl;
        }
    }
}

void UserInterface::showField(GameField& gameField) {
	int gameFieldSize = gameField.size();
    for (int y = 0; y < gameFieldSize; y++) {
		if (y != 0)
		{
			for (int index = 0; index < (2 * ((gameFieldSize * 2))) - 1; index++)
			{
				if ((index % 4) == 3)
				{
					std::cout << "+";
				}
				else
				{
					std::cout << "-";
				}
			}
		}
		std::cout << std::endl;
        for (int x = 0; x < gameFieldSize; x++) {
			if (x != 0)
            {
				std::cout << " | ";
			}
			if (x == 0)
			{
				std::cout << " " << gameField[Position(x, y)].getColorAsLetter();
			}
			else {
				std::cout << gameField[Position(x, y)].getColorAsLetter();
			}
        }
		std::cout << std::endl;
    }


}

auto UserInterface::getUserInputBubble() -> Position
{
	int x, y;
	std::cout << "Spalte: ";
	std::cin >> x;
	std::cout << "Zeile: ";
	std::cin >> y;
	Position p = Position(x, y);
	if (p <= gameField_.size() && p >= 1)
	{
		return p - 1;
	}
	else
	{
		throw CCException("Falsche Eingabe");
	}
}
