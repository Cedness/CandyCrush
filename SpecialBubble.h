//
// Created by Ced on 17/07/2021.
//

#ifndef CANDYCRUSH_SPECIALBUBBLE_H
#define CANDYCRUSH_SPECIALBUBBLE_H


#include "Bubble.h"

/// Base of the special bubbles. Only thing it does is overriding hasSpecialAction() for convenience, so that not every special bubble has to do it
class SpecialBubble : public Bubble {
public:
    SpecialBubble(Position position, Color color);
protected:
    auto hasSpecialAction() const -> bool override;
};


#endif //CANDYCRUSH_SPECIALBUBBLE_H
