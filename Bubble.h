//
// Created by ced on 28.05.21.
//
#include "GameField.h"

#ifndef CANDYCRUSH_BUBBLE_H
#define CANDYCRUSH_BUBBLE_H


class Bubble;

#include "Color.h"
#include "GameField.h"
#include "BubbleContainer.h"
#include "Position.h"
#include "ClickResult.h"
#include <vector>
#include <utility>

/// Represents a candy-crush bubble
class Bubble {
    friend class GameField;
    friend class BubbleContainer;
public:
    /// Creates a bubble
    /// \param position initial position
    /// \param color initial color
    Bubble(Position position, Color color);
    virtual ~Bubble();
    /// \return the color of the bubble
    auto getColor() const -> Color;
    /// \return the letter representing the color of this bubble
    auto getColorAsLetter() const -> char;
    /// \return the position of this bubble
    auto getPosition() const -> Position;
protected:
    /// \return @true if this is a special bubble with special actions when clicked on it
    virtual auto hasSpecialAction() const -> bool;
    /// \return @true if the special action requires to steps, meaning another bubble, to work
    virtual auto isTwoStepAction() const -> bool;
    /// Performs the special action of this bubble. Only to be called if hasSpecialAction() returns true and isTwoStepAction() returns false
    /// \param gameField the game of this bubble
    /// \return the points earned and the deleted bubbles
    virtual auto performFirstClickAction(GameField& gameField) -> std::pair<double, std::vector<Deletion>>;
    /// Performs the special action of this bubble. Only to be called if hasSpecialAction() returns true and isTwoStepAction() returns true
    /// \param gameField the game of this bubble
    /// \return the points earned and the deleted bubbles
    virtual auto performSecondClickAction(GameField& gameField, Bubble& firstClick) -> std::pair<double, std::vector<Deletion>>;
private:
    /// Sets the position of this bubble
    /// \param position new position
    void setPosition(Position position);
    /// Position of this bubble
    Position position_;
    /// Color of this bubble
    Color color_;
};


#endif //CANDYCRUSH_BUBBLE_H
