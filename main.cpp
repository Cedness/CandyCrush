#include "GameField.h"
#define QT true // Set to false if you want to use the console-version
#if QT
#include "MainWindow.h"
#include "CCException.h"
#include <QtWidgets/QApplication>
#else
#include "UserInterface.h"
#endif
#define EXTRA_CONSOLE false // Set to true if you cannot see debug log. Works only on Windows
#if EXTRA_CONSOLE
#include <Windows.h>
#endif

int main(int argc, char* argv[]) {
#if EXTRA_CONSOLE
    AllocConsole();
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);
#endif

#if QT
    int state = 0;
    try {
        QApplication app(argc, argv);
        MainWindow win(&app);
        win.show();
        state = QApplication::exec();
    } catch (CCException& ex) {
        std::cout << ex.what() << std::endl;
    }
    return state;
#else
    UserInterface ui_(5);
    ui_.runGame();
    return 0;
#endif
}
