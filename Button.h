//
// Created by Ced on 29/07/2021.
//

#ifndef CANDYCRUSH_BUTTON_H
#define CANDYCRUSH_BUTTON_H


class Button;

#include "MainWindow.h"
#include <memory>
#include <QPushButton>
#include <QPropertyAnimation>
#include <QEasingCurve>

/// Represents a QT-Button
class Button {
    /// Easing curve used for the animations
    static const QEasingCurve easingCurve_;
    /// rgb-values used for colors
    static const std::array<QRgb, COLOR_SIZE> rgbColors;
public:
    /// Construct a button with an animation
    /// \param window the game window
    Button(MainWindow* window);
    /// \return the QPushButton pointer
    auto button() -> QPushButton*;
    /// Change the color
    /// \param color new color
    void setColor(Color color);
    /// "Teleport" to a position
    /// \param position where to go
    void setPosition(Position position);
    /// Animate the transition to a position
    /// \param position where to go
    void wooooshPosition(Position position);
private:
    /// Game window
    MainWindow* window_;
    /// QT's button pointer
    std::unique_ptr<QPushButton> button_;
    /// Animation
    std::unique_ptr<QPropertyAnimation> animation_;
};


#endif //CANDYCRUSH_BUTTON_H
