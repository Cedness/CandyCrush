//
// Created by Ced on 26/07/2021.
//

#include "Refill.h"

Refill::Refill(Position from, Position to, Color color)
        : from_(from), to_(to), color_(color) {}

Position Refill::getSource() const {
    return from_;
}

Position Refill::getTarget() const {
    return to_;
}

Color Refill::getColor() const {
    return color_;
}
