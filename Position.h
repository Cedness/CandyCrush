//
// Created by ced on 28.05.21.
//

#ifndef CANDYCRUSH_POSITION_H
#define CANDYCRUSH_POSITION_H


#include <cstddef>
#include <string>
#include <array>
#include <ostream>

/// Represents a position consisting of two ints
class Position {
public:
    Position();
    Position(int x, int y);
    auto x() const -> int;
    void x(int x);
    auto y() const -> int;
    void y(int y);
    auto operator-() const -> Position;
    auto operator+(Position rhs) const -> Position;
    auto operator-(Position rhs) const -> Position;
    auto operator*(Position rhs) const -> Position;
    auto operator+(int value) const -> Position;
    auto operator-(int value) const -> Position;
    auto operator+=(Position rhs) -> Position;
    auto operator-=(Position rhs) -> Position;
    auto abs() const -> Position;
    auto operator==(const Position& rhs) const -> bool;
    auto operator!=(const Position& rhs) const -> bool;
    auto operator==(int value) const -> bool;
    auto operator!=(int value) const -> bool;
    auto operator<(int value) const -> bool;
    auto operator>(int value) const -> bool;
    auto operator<=(int value) const -> bool;
    auto operator>=(int value) const -> bool;
    auto lengthNaive() const -> int;
    auto lengthSquared() const -> int;
    auto length() const -> double;
    auto isValid() const -> bool;
    auto inBounds(int gameFieldSize) const -> bool;
    auto toIndex(int gameFieldSize) const -> int;
    static const Position INVALID;
    static const std::array<Position, 4> DIRECTIONS;
private:
    int x_;
    int y_;
};

auto operator<<(std::ostream& ostream, const Position& position) -> std::ostream&;


#endif //CANDYCRUSH_POSITION_H
