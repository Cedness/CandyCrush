//
// Created by Ced on 16/07/2021.
//

#ifndef CANDYCRUSH_SQUAREBUBBLE_H
#define CANDYCRUSH_SQUAREBUBBLE_H


#include "SpecialBubble.h"

class SquareBubble : public SpecialBubble {
public:
    SquareBubble(Position position);
protected:
    auto performFirstClickAction(GameField& gameField) -> std::pair<double, std::vector<Deletion>> override;
};


#endif //CANDYCRUSH_SQUAREBUBBLE_H
