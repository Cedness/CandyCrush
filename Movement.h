//
// Created by Ced on 26/07/2021.
//

#ifndef CANDYCRUSH_MOVEMENT_H
#define CANDYCRUSH_MOVEMENT_H


#include "Change.h"

class Movement : public Change {
public:
    Movement(Position from, Position to);
    auto getSource() const -> Position override;
    auto getTarget() const -> Position override;
private:
    Position from_, to_;
};


#endif //CANDYCRUSH_MOVEMENT_H
