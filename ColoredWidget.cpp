#include "ColoredWidget.h"

ColoredWidget::ColoredWidget(const QColor& color, QWidget* parent) : QWidget(parent)
{
    QPalette pal;
    QBrush brush(color);
    brush.setStyle(Qt::SolidPattern);
    pal.setBrush(QPalette::Active, QPalette::Window, brush);
    setPalette(pal);
    setAutoFillBackground(true);
}