//
// Created by Ced on 26/07/2021.
//

#ifndef CANDYCRUSH_COLOR_H
#define CANDYCRUSH_COLOR_H


/// Represents the colors
enum Color {
    BLUE = 0, GREEN, RED, PINK, YELLOW, LIGHT_GRAY, DARK_GRAY, BLACK
};

/// Size of enum without special colors
#define DEFAULT_COLOR_SIZE (static_cast<int>(Color::YELLOW + 1))
#define COLOR_SIZE (static_cast<int>(Color::BLACK + 1))

auto colorAsLetter(Color color) -> char;


#endif //CANDYCRUSH_COLOR_H
