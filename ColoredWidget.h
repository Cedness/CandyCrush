#ifndef CANDYCRUSH_COLOREDWIDGET_H
#define CANDYCRUSH_COLOREDWIDGET_H


#include <QtWidgets/QMainWindow>


/// A widget with color
class ColoredWidget : public QWidget
{
public:
    ColoredWidget(const QColor& color, QWidget* parent);
};


#endif //CANDYCRUSH_COLOREDWIDGET_H
