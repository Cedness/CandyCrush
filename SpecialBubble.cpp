//
// Created by Ced on 17/07/2021.
//

#include "SpecialBubble.h"

SpecialBubble::SpecialBubble(Position position, Color color)
        : Bubble(position, color) {}

auto SpecialBubble::hasSpecialAction() const -> bool {
    return true;
}
