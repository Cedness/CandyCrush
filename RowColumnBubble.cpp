//
// Created by Ced on 17/07/2021.
//

#include "RowColumnBubble.h"

RowColumnBubble::RowColumnBubble(Position position, bool columnMode) : SpecialBubble(position, DARK_GRAY), columnMode_(columnMode) {}

auto RowColumnBubble::performFirstClickAction(GameField& gameField) -> std::pair<double, std::vector<Deletion>> {
    std::vector<Deletion> deletions;
    deletions.reserve(gameField.size());
    for (int i = 0; i < gameField.size(); i++) {
        deletions.emplace_back(Position(columnMode_ ? getPosition().x() : i, columnMode_ ? i : getPosition().y()));
    }
    return { 4, deletions };
}
