//
// Created by Ced on 29/07/2021.
//

#include "Button.h"

const QEasingCurve Button::easingCurve_ = QEasingCurve(QEasingCurve::InOutQuad);

const std::array<QRgb, COLOR_SIZE> Button::rgbColors = std::array<QRgb, COLOR_SIZE>{
    qRgb(0, 191, 255),
    qRgb(0, 205, 0),
    qRgb(255, 48, 48),
    qRgb(255, 20, 147),
    qRgb(255, 255, 0),
    qRgb(211, 211, 211),
    qRgb(105, 105, 105),
    qRgb(0, 0, 0)
};

Button::Button(MainWindow* window)
        : window_(window), button_(std::make_unique<QPushButton>(window)), animation_(std::make_unique<QPropertyAnimation>(button_.get(), "geometry")) {
    button_->setFixedSize(window->buttonSize(), window->buttonSize());
    animation_->setDuration(230);
    animation_->setEasingCurve(easingCurve_);
}

auto Button::button() -> QPushButton* {
    return button_.get();
}

void Button::setColor(Color color) {
    QColor col(rgbColors[color]);

    if (col.isValid()) {
        QString qss = QString("background-color: %1").arg(col.name());
        button_->setStyleSheet(qss);
    }
}

void Button::setPosition(Position position) {
    int padding = window_->padding();
    int size = window_->buttonSize();
    int offset = window_->offset();
    button_->setGeometry(position.x() * padding + offset, position.y() * padding + offset, size, size);
}

void Button::wooooshPosition(Position position) {
    int padding = window_->padding();
    int size = window_->buttonSize();
    int offset = window_->offset();
    animation_->setStartValue(button_->geometry());
    animation_->setEndValue(QRect(position.x() * padding + offset, position.y() * padding + offset, size, size));
    animation_->start();
}
