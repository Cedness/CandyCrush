//
// Created by ced on 28.05.21.
//

#ifndef CANDYCRUSH_CHANGE_H
#define CANDYCRUSH_CHANGE_H


class Change;

#include "Position.h"
#include "Color.h"
#include <ostream>

class Change {
public:
    virtual auto getSource() const -> Position;
    virtual auto getTarget() const -> Position;
    virtual auto getColor() const -> Color;
};

auto operator<<(std::ostream& ostream, const Change& change) -> std::ostream&;


#endif //CANDYCRUSH_CHANGE_H
