//
// Created by ced on 28.05.21.
//

#include "Change.h"

auto Change::getSource() const -> Position {
    return Position::INVALID;
}

auto Change::getTarget() const -> Position {
    return Position::INVALID;
}

Color Change::getColor() const {
    return static_cast<Color>(0);
}

auto operator<<(std::ostream& ostream, const Change& change) -> std::ostream& {
    return ostream << change.getSource() << "->" << change.getTarget();
}
